#ifndef GEOMETRY_POLYGONS_AREA_H_
#define GEOMETRY_POLYGONS_AREA_H_

#include "geometry/polygons/polygon.pb.h"

namespace geometry {
namespace polygons {

double polygonArea(const Polygon &polygon);

}  // namespace polygons
}  // namespace geometry

#endif
