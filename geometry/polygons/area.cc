#include "geometry/polygons/area.h"
#include "geometry/polygons/convex.h"

#include "glog/logging.h"

namespace geometry {
namespace polygons {

double polygonArea(const Polygon &polygon) {
  CHECK(is_convex(polygon)) << "Polygon must be convex!";

  double sum = 0.0;
  for (int i = 1; i < polygon.vertices().size() - 1; i++) {
    sum += crossProduct(polygon.vertices(0), polygon.vertices(i),
                        polygon.vertices(i + 1)) /
           2;
  }

  return sum;
}

}  // namespace polygons
}  // namespace geometry
