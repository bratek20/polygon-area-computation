#include "geometry/polygons/convex.h"

#include "glog/logging.h"

namespace geometry {
namespace polygons {

namespace {

bool turns_left(const Point& a, const Point& b, const Point& c) {
  // We use dot-product to detect if the point c lies left of the line a-b.
  return crossProduct(a, b, c) > 0;
}

}  // namespace

double crossProduct(const Point& a, const Point& b, const Point& c) {
  return ((b.x() - a.x()) * (c.y() - a.y()) -
          (b.y() - a.y()) * (c.x() - a.x()));
}

bool is_convex(const Polygon& polygon) {
  CHECK_GE(polygon.vertices().size(), 3)
      << "Polygon must have at least 3 vertices.";
  for (int i = 0; i < polygon.vertices().size(); i++) {
    if (!turns_left(polygon.vertices(i),
                    polygon.vertices((i + 1) % polygon.vertices().size()),
                    polygon.vertices((i + 2) % polygon.vertices().size()))) {
      LOG(INFO) << "Points " << i << " - " << i + 2 << " form a right turn.";
      return false;
    }
  }
  return true;
}

}  // namespace polygons
}  // namespace geometry
